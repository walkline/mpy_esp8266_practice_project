import wifi_handler
from machine import Timer
import webserver

def check_client_request_cb(timer):
	pass


if (__name__ == "__main__"):
	try:
		wifi_handler.set_ap_mode()

		web_server = webserver.WebServer()
		web_server.init()

		web_server_timer = Timer(0)
		web_server_timer.init(
			mode=Timer.PERIODIC,
			period=100,
			callback=web_server.check_client_request_cb
		)
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET DEVICE")