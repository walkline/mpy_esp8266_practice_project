from utime import sleep
from Chip74HC595 import Chip74HC595

lists = ['00000001', '00000010', '00000100', '00001000', '00010000', '00100000', '01000000', '10000000']

if __name__ == "__main__":
	chip = Chip74HC595()

	for i in range(10):
		for count in range(8):
			chip.set_bit_data(lists[count])

			sleep(0.2)

		chip.clear()
		sleep(0.5)

		for count in range(7, -1, -1):
			chip.set_bit_data(lists[count])

			sleep(0.2)

		chip.clear()
		sleep(0.5)
