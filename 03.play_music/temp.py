bpm = 296          # bpm = 每分钟节拍，可更改


def tone(pin, note):
    pass


def delay(time):
    pass


def dura(x):                      # 时值函数，分别为全音符，二分音符，四分音符，八分音符，十六分音符，三十二分音符，六十四分音符，想到还可以加
    dura = {240000/bpm, 120000/bpm, 60000/bpm, 30000/bpm, 15000/bpm, 7500/bpm, 3750/bpm}
                                              # （忘了怎么设计的...）定义时值数组，时值的计算公式是：  
    delay(dura[x-1])                # 延时
    rest(8)                               # rest(pin)是自定义的休止符函数，之后有讲
    delay(50)                          # 这里加一个50ms的短暂休止是模仿钢琴弹奏时音符与音符切换时的间隙

 
def durac(x):                   # 不带短暂休止的时值函数，用于有连音和音符时值较长的时候
    durac = {240000/bpm, 120000/bpm, 60000/bpm, 30000/bpm, 15000/bpm, 7500/bpm, 3750/bpm}
    delay(durac[x-1])


def withdot(x):                 # 附点音符函数，我是把音阶和时值分由两种不同的函数控制
   durac[x]                             # 附点音符的时值是原时值 #1.5；
   dura[x+1]

                                             # 接下来就是音阶部分，七个全音阶，五个半音阶，从C1到B7，总计84个音，全在听觉范围内
                                             # 其实音阶就是声音频率，不同频率产生不同的音阶，根据网上的音阶频率表设计
                                             # 目前仅有部分音符记号函数，如连音和附点，重音等暂无法实现                                             # 循环的话，目前就只靠复制代码解决（0 w 0 )~


def do_ut(pin, x):        # 音名C（或B#），唱名do，用“do_ut"表示（由于do为保留字），音阶分别为C1，C2，C3，C4，C5，C6，C7
    C = {33, 65, 131, 262, 523, 1047, 2093}
    tone(pin, C[x-1])                # tone()函数，是pin口发出特定频率的声音信号，以下略


def do_up(pin, x):       # 音名C#，音阶分别为C#1，C#2，C#3，C#4，C#5，C#6，C#7
    CU = {35, 69, 139, 277, 554, 1109, 2217}
    tone(pin, CU[x-1])

 
def re_dw(pin, x):        # 音名Db，音阶分别为Db1，Db2，Db3，Db4，Db5，Db6，Db7
    DD = {35, 69, 139, 277, 554, 1109, 2217}
    tone(pin, DD[x-1])

 
def re(pin, x):              # 音名D，唱名re，音阶分别为D1，D2，D3，D4，D5，D6，D7
    D = {37, 73, 147, 294, 587, 1175, 2349}
    tone(pin, D[x-1])

 
def re_up(pin, x):        # 音名D#，音阶分别为D#1，CD#2，D#3，D#4，D#5，D#6，D#7
    DU = {39, 78, 156, 311, 622, 1245, 2489}
    tone(pin, DU[x-1])

 
def mi_dw(pin, x):      # 音名Eb，音阶分别为Eb1，Eb2，Eb3，Eb4，Eb5，Eb6，Eb7
    ED = {39, 78, 156, 311, 622, 1245, 2489}
    tone(pin, ED[x-1])

 
def mi(pin, x):            # 音名E，唱名mi，音阶分别为E1，E2，E3，E4，E5，E6，E7
    E = {41, 82, 165, 330, 659, 1319, 2637}
    tone(pin, E[x-1])

 
def fa(pin, x):             # 音名F，唱名fa，音阶分别为F1，F2，F3，F4，F5，F6，F7
    F = {44, 87, 175, 349, 698, 1397, 2794}
    tone(pin, F[x-1])

 
def fa_up(pin, x):       # 音名F#，音阶分别为F#1，F#2，F#3，F#4，F#5，F#6，F#7
    FU = {46, 93, 185, 370, 740, 1480, 2960}
    tone(pin, FU[x-1])

 
def sol_dw(pin, x):     # 音名Gb，音阶分别为Gb1，Gb2，Gb3，Gb4，Gb5，Gb6，Gb7
    GD = {46, 93, 185, 370, 740, 1480, 2960}
    tone(pin, GD[x-1])

 
def sol(pin, x):           # 音名G，唱名sol，音阶分别为G1，G2，G3，G4，G5，G6，G7
    G = {49, 98, 196, 392, 784, 1568, 3136}
    tone(pin, G[x-1])

 
def sol_up(pin, x):     # 音名G#，音阶分别为G#1，G#2，G#3，G#4，G#5，G#6，G#7
    GU = {52, 104, 208, 415, 831, 1661, 3322}
    tone(pin, GU[x-1])

 
def la_dw(pin, x):      # 音名Ab，音阶分别为Ab1，Ab2，Ab3，Ab4，Ab5，Ab6，Ab7
    AD = {52, 104, 208, 415, 831, 1661, 3322}
    tone(pin, AD[x-1])

 
def la(pin, x):            # 音名A，唱名do，音阶分别为A1，A2，A3，A4，A5，A6，A7
    A = {55, 110, 220, 440, 880, 1760, 3520}
    tone(pin, A[x-1])

 
def la_up(pin, x):      # 音名A#，唱名do，音阶分别为A#1，A#2，A#3，A#4，A#5，A#6，A#7
    AU = {58, 117, 233, 466, 932, 1865, 3729}
    tone(pin, AU[x-1])

 
def si_dw(pin, x):      # 音名Bb，唱名do，音阶分别为Bb1，Bb2，Bb3，Bb4，Bb5，Bb6，Bb7
    BD = {58, 117, 233, 466, 932, 1865, 3729}
    tone(pin, BD[x-1])

 
def si(pin, x):            # 音名B，唱名do，音阶分别为B1，B2，B3，B4，B5，B6，B7
    B = {62, 123, 247, 494, 988, 1976, 3951}
    tone(pin, B[x-1])

 
def rest(pin):               # 休止符，tone()的频率设为零就行了
    tone(pin,  0)

 
def setup():
    # Attention:                     # 这里再次对时值的表示方法做解释
    # whole = 1                     # 全音符
    # half = 2                         # 二分音符
    # quarter = 3                   # 四分音符
    # eighth = 4                    # 八分音符
    # sixteenth = 5                # 十六分音符
    # thirtysecond = 6          # 三十二分音符
    # sixtyforth = 7               # 六十四分音符
    pass


def loop():
    # Here's an example:    # 自己写的一个例子：小星星
    # do_ut(8, 4) dura(3)
    # do_ut(8, 4) dura(3)
    # sol(8, 4)   dura(3)
    # sol(8, 4)   dura(3)
    # la(8, 4)    dura(3)
    # la(8, 4)    dura(3)
    # sol(8, 4)   dura(2)
    # rest(8)
    # delay(50)
    pass
