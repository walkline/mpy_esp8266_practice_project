from config import Config
from time import sleep

def connect_to_internet():
	import network

	sta_if = network.WLAN(network.STA_IF)
	sta_if.active(False)
	sta_if.active(True)

	print("\nConnecting to internet...")

	sta_if.connect(Config.WIFI_SSID, Config.WIFI_PASSWORD)

	while (not sta_if.isconnected()):
		result_code = sta_if.status()

		if (result_code == network.STAT_IDLE):
			break
		elif (result_code == network.STAT_CONNECT_FAIL):
			break
		elif (result_code == network.STAT_CONNECTING):
			pass
		elif (result_code == network.STAT_GOT_IP):
			break
		elif (result_code == network.STAT_NO_AP_FOUND):
			break
		elif (result_code == network.STAT_WRONG_PASSWORD):
			break

		sleep(0.5)

	result_code = sta_if.status()

	if (result_code == network.STAT_IDLE):
		print("network idle")
	elif (result_code == network.STAT_CONNECT_FAIL):
		print("network connect failed")
	elif (result_code == network.STAT_GOT_IP):
		pass
	elif (result_code == network.STAT_NO_AP_FOUND):
		print("cannot found ap")
	elif (result_code == network.STAT_WRONG_PASSWORD):
		print("wrong password given")

	if (result_code == network.STAT_GOT_IP):
		print("- network config:", sta_if.ifconfig(), end='\n\n')

		return True

	return False
