from machine import Pin
from mqtt import mqtt_lient
import connection


if (__name__ == "__main__"):
	led = Pin(2, Pin.OUT, value=1)

	try:
		if (connection.connect_to_internet()):
			client = mqtt_lient(led)
			client.start()
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET DEVICE")