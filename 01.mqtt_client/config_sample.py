class Config:
	# OneNet MQTT Host IP
	MQTT_HOST = "183.230.40.39"
	MQTT_HOST_PORT = 6002

	# Device ID
	MQTT_CLIENT_ID = ""

	# Product Name
	MQTT_USERNAME = ""

	# Authentication Info
	MQTT_PASSWORD = ""

	MQTT_TOPIC = b'led_notice'

	WIFI_SSID = ""
	WIFI_PASSWORD = ""
