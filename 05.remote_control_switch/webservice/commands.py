class Commands:
	QUERY_PREFIX = "query"
	QUERY_CONTROL_PREFIX = "control"

	# ==== tcp commands ===============================
	"""
		# query command example:
		# 	querySetupStation?essid=xxxx&password=xxxx
	"""
	# receive essid & password for station mode
	CMD_SETUP_STATION = "querySetupStation"
	# =================================================

	# ==== udp commands ===============================
	"""
		# query command example:
		#	queryOnlineStatus
		#	controlPowerOn?mac=0x0x0x
		#	controlPowerOff?mac=0x0x0x
		#	controlTogglePower?mac=0x0x0x
	"""
	# whether device online or not
	CMD_QUERY_ONLINE_STATUS = "queryOnlineStatus"

	# device type: [ONE_SWITCH]
	CMD_CONTROL_OS_POWER_ON = "controlPowerOn"
	CMD_CONTROL_OS_POWER_OFF = "controlPowerOff"
	CMD_CONTROL_OS_TOGGLE_POWER = "controlTogglePower"
	# =================================================

	# ==== tcp results ================================
	RESULT_SETUP_STATION_SUCCESS = "result=success&mac=%s"
	RESULT_SETUP_STATION_FAILED = "result=failed&msg=station_connect_failed"

	RESULT_INVALID_PARAMS = "result=failed&msg=invalid_params"
	RESULT_INVALID_COMMAND = "result=failed&msg=invalid_command"
	# =================================================

	# ==== udp results ================================
	RESULT_QUERY_ONLINE_STATUS_SUCCESS = "result=success&mac=%s"
	RESULT_CONTROL_OS_POWER_ON_SUCCESS = RESULT_QUERY_ONLINE_STATUS_SUCCESS
	RESULT_CONTROL_OS_POWER_OFF_SUCCESS = RESULT_QUERY_ONLINE_STATUS_SUCCESS
	RESULT_CONTROL_OS_TOGGLE_POWER_SUCCESS = RESULT_QUERY_ONLINE_STATUS_SUCCESS

	RESULT_CONTROL_OS_POWER_ON_FAILED = "result=failed&mac=%s"
	RESULT_CONTROL_OS_POWER_OFF_FAILED = RESULT_CONTROL_OS_POWER_ON_FAILED
	RESULT_CONTROL_OS_TOGGLE_POWER_FAILED = RESULT_CONTROL_OS_POWER_ON_FAILED
	# =================================================

	@staticmethod
	def get_command(request: str):
		"""
		return requested control command
		:param request: client request command
		"""

		return request.split("?")[0]

	@classmethod
	def get_params(cls, request: str):
		"""
		according to request command return params
		:param request: client request command
		"""

		try:
			command, params = request.split("?")
			params = cls._parse_params(params)

			if command == cls.CMD_SETUP_STATION:
				return params["essid"], params["password"]
		except KeyError:
			return False

	@classmethod
	def get_mac_address(cls, request: str):
		try:
			params = request.split("?")[1]
			params = cls._parse_params(params)

			return params["mac"]
		except KeyError:
			return False

	@staticmethod
	def _parse_params(params: str):
		result_list = {}
		params = params.split("&")

		for param in params:
			key_value = param.split("=")
			result_list[key_value[0]] = key_value[1]

		return result_list
