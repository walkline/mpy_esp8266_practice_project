class Product:
	DEVICE_NAME = "[WKHW]"
	DEVICE_TYPE = "[ONE_SWITCH]"

	CAPABILITY = {DEVICE_TYPE: ["controlPowerOn", "controlPowerOff", "controlTogglePower"]}

	def __init__(self):
		pass

	@classmethod
	def name(cls):
		return cls.DEVICE_NAME + cls.DEVICE_TYPE
