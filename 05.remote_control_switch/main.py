import utils
from wifihandler import WifiHandler
from webservice._tcp_server import TCPServer
from webservice._udp_server import UDPServer


if __name__ == "__main__":
	try:
		# if information file exists, enter station mode
		# and start udp server used to receive control command
		if utils.is_info_file_exist():
			if type(utils.get_information()) is not bool:
				essid, password = utils.get_information()

				result_code = WifiHandler.set_sta_mode(essid, password)
				WifiHandler.set_ap_status(False)

				# start udp server
				if result_code == 5:
					udp_server = UDPServer()
					udp_server.start()
			else:
				raise IOError("can not read information file")
		# otherwise, enter ap mode and start tcp server
		else:
			WifiHandler.set_ap_mode()
			WifiHandler.set_sta_status(False)

			# start tcp server
			tcp_server = TCPServer()
			tcp_server.start()
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET NOW!")
