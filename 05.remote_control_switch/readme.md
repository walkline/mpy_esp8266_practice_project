# 远程控制开关
使用安卓（client）控制由ESP8266（server）充当的电源开关

## 使用流程
暂无

## 工作流程
> MicroPython 并未实现 SmartConfig 或者 AirKiss，所以需要自己实现一键配网功能

1. `Server`开启 AP 等待`Client`扫描，essid 需要加上特殊关键字
2. `Server`启动 TCP Server，用于接收配网所需的信息，并给`Client`反馈配网结果
3. `Client`扫描周围的 WIFI，根据 essid 名称中的关键字进行过滤，只保留所需的`设备列表`
4. `Client`点击`设备列表`中的某一项，填写配网信息并完成配网
5. `Client`配网成功后根据`Server`的 MAC 地址 将 `Server`标记为`已添加的设备`
6. `Server`根据配网信息连接路由器，并反馈给`Client`配网结果
7. 如果配网成功则把配网信息以文件形式保存到`Server`上，并重启`Server`
8. `Server`启动时如果找到配网信息文件则以 STA 模式连接到路由器，并启动 UDP Server，用以接收来自`Client`的控制命令
9. `Client`的控制命令包括
	- 查询设备在线状态：以广播形式发送命令，`Server`做出响应代表在线，长时间无响应代表离线；
	- 根据`Server`提供的能力列表发送相应的控制命令
10. `Client`从`已添加的设备`列表中删除某个设备时，`Server`删除配网信息文件并反馈删除结果，延时后重启，回到步骤1状态

### Server 所需功能模块
- 首先

### Client 实现流程
- 首先
