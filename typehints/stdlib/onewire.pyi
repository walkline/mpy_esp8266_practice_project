class OneWire:
	def __init__(self, object):
		"""
		"""
		...

	def scan(self):
		"""
		"""
		...

	def reset(self):
		"""
		"""
		...

	def readbyte(self):
		"""
		"""
		...

	def writebyte(self, data):
		"""
		"""
		...

	def write(self, data: bytes):
		"""
		"""
		...

	def select_rom(self, rom: bytes):
		"""
		"""
		...
