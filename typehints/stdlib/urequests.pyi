class Response:
    def __init__(self, raw):
		"""
		"""
		...

    def close(self):
		"""
		"""
		...

    def content(self):
		"""
		"""
		...

    def text(self):
		"""
		"""
		...

    def json(self):
		"""
		"""
		...


def request(method, url, data=None, json=None, headers={}, stream=None):
	"""
	"""
	...


def head(url, **kw):
	"""
	"""
	...

def get(url, **kw):
	"""
	"""
	...

def post(url, **kw):
	"""
	"""
	...

def put(url, **kw):
	"""
	"""
	...

def patch(url, **kw):
	"""
	"""
	...

def delete(url, **kw):
	"""
	"""
	...
