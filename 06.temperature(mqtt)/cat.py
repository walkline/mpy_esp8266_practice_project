import os

_EXCLUDE_LIST = ['[.idea]']


def listdir(parent=".", prefix="", collapse=True):
	try:
		for file_name in os.listdir(parent):
			file_fullname = parent + "/" + file_name

			if _is_dir(file_fullname):
				file_name = "[" + file_name + "]"

			print(prefix + file_name)

			if file_name not in _EXCLUDE_LIST:
				if collapse:
					listdir(file_fullname, prefix + "\t")
	except OSError as ose:
		if str(ose) == "[Errno 2] ENOENT":
			pass


def _is_dir(path):
	try:
		os.listdir(path)

		return True
	except OSError:
		return False


def usage():
	print(
		"""
	Cat is a tool used to list all files and folders, also view file contents.

	Usage:
		cat.usage() or cat.cat()
			- show this message
			
		cat.cat("filename")
			- show file contents
			
		cat.listdir()
			- show all files and folders
			
		cat.listdir("sub_folder")
			- show all files and folders in sub folder
			
		cat.listdir(collapse=False)
			- show all files and folders, but don't extend sub folders
		"""
	)


usage()


def cat(filename=""):
	if filename.strip() != "":
		try:
			with open(filename, "r", encoding="utf-8") as file:
				print(file.read())
		except OSError:
			print("ERROR: File [%s] is not exist\n\nUse cat.listdir() to show all files" % filename)
	else:
		usage()
