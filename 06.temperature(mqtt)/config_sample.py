class Config:
	WIFI_SSID = ""
	WIFI_PASSWORD = ""

	DEBUG = True

	class MQTT:
		"""
		OneNet MQTT Information
		"""

		# OneNet MQTT Host IP
		HOST = "183.230.40.39"
		PORT = 6002

		# Device ID
		CLIENT_ID = ""

		# Product ID
		USERNAME = ""

		# Authentication Info
		PASSWORD = ""

		TOPIC = b'wkhw/sensor/temperature'
