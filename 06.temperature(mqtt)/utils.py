def soft_reset():
	"""
	a soft reset
	"""

	import sys

	sys.exit()


def hard_reset():
	import machine

	machine.reset()


# def update_temperature(temp):
# 	"""
# 	update data to web server
# 	"""
#
# 	import ujson
# 	import urequests
# 	from config import Config
#
# 	data = Config.POST_DATA % temp
#
# 	try:
# 		response = urequests.post(Config.UPDATE_TEMPERATURE_URI, data=data, headers=Config.HEADERS)
#
# 		if response.text:
# 			result = ujson.loads(response.text)
#
# 			if result[Config.RESULT] == Config.ERROR_CODE:
# 				debug_info(result[Config.ERROR_MSG], "utf-8")
# 	except Exception as e:
# 		print(e)


def debug_info(msg):
	"""
	print message if in DEBUG mode
	"""

	from config import Config

	if Config.DEBUG:
		if isinstance(msg, str) or isinstance(msg, tuple):
			print(msg)
		else:
			print(str(msg, "utf-8"))
