import timercontroller
from wifihandler import WifiHandler
from config import Config


if __name__ == "__main__":
	timerController = timercontroller.TimerController()

	try:
		timerController.start_wifi_status_timer()

		result_code = WifiHandler.set_sta_mode(Config.WIFI_SSID, Config.WIFI_PASSWORD)

		if result_code == 5:
			timerController.start_get_temperature_timer()  # period=5000)

			import webrepl

			webrepl.start()  # 192.168.11.30:8266
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET DEVICE")

		timerController.deinit()
