/*
Navicat MySQL Data Transfer

Source Server         : remote
Source Server Version : 50148
Source Host           : qdm16522623.my3w.com:3306
Source Database       : qdm16522623_db

Target Server Type    : MYSQL
Target Server Version : 50148
File Encoding         : 65001

Date: 2019-01-15 23:37:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for temperature_recorders
-- ----------------------------
DROP TABLE IF EXISTS `temperature_recorders`;
CREATE TABLE `temperature_recorders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temperature1` varchar(8) DEFAULT NULL,
  `temperature2` varchar(8) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  `device_id` char(15) DEFAULT NULL,
  `datastream_id` char(30) DEFAULT NULL,
  `device_status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
