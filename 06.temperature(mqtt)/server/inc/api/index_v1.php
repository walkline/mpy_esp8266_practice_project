<?php
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');

	include_once('../connect2db.php');
	
	if ($_SERVER['REQUEST_METHOD'] === 'GET') {
		switch ($_REQUEST['v1']) {
			case 'getPlotData':
				$returnObject = getPlotData(@$_GET['range']);

				break;
			default:
				$returnObject = (object) array(
					"error_code" => 2000,
					"error_msg" => "未知的api接口",
				);
		}

		echo json_encode($returnObject);
	}

	function getPlotData($range) {
		if (!$range) {$range = 1;}

		$returnObject = array();

		$result = mysql_query(
								"SELECT " .
									"recorders.temperature1," .
									"recorders.temperature2," .
									"recorders.`timestamp`" .
								" FROM " .
									"temperature_recorders AS recorders" .
								" WHERE " .
									"recorders.`timestamp` > DATE_SUB(NOW(), INTERVAL $range HOUR)" .
								" AND " .
									"recorders.temperature1 IS NOT NULL"
							);

		//$count = 0;

		$record_lists;
		
		while ($row = mysql_fetch_assoc($result)) {
			$list = array(
				$row['timestamp'],
				$row['temperature1'],
				$row['temperature2']
			);

			$record_lists[] = array("value" => $list);
		}

		$returnObject = array(
			"result" => "success",
			"data" => $record_lists
		);
		
		return $returnObject;
	}
?>