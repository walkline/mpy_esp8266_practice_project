"use strict"

$(document).ready(function () {
	$('#user-nav').load('interface/template/user-nav');

	$.get(DATABASESERVICEURI + "getDevice", {}, function (result) {
		var lis = "";
		$.each(result.lists, function (index, list) {
			lis += '<li class="offline" id="' + list.IP + '"><i class="icon-cog"></i><small>' + list.className2 + '</small></li>';
		});

		$('.device-status').html(lis);

		refreshDeviceStatus();
		//autoDownloadRecorders();
	});

	var refreshDeviceStatus = function () {
		$.each($('.device-status li'), function (index, item) {
			$.get(WEBSERVICEURI + "getStatus", {ip: item.id}, function (result) {
				if (result.status === "online") {
					$(item).removeClass('offline');
					$(item).addClass('online');
				} else {
					$(item).removeClass('online');
					$(item).addClass('offline');
				}
			});
		});
	};

	var autoDownloadRecorders = function () {
		var total_count = 0;

		$.get(DATABASESERVICEURI + "getDevice", {available: "available"}, function (result) {
			$.each(result.lists, function (index, list) {
				$(document).queue("ajaxRequests", function () {
					$.post(WEBSERVICEURI + "syncRecorders", {ip: list.IP, className: list.className, classCode: list.classCode}, function (result) {
						total_count += parseInt(result.count);

						$(document).dequeue("ajaxRequests");
					})
				});
			});

			$(document).queue("ajaxRequests", function () {
				//alert("同步设备刷卡记录完成，本次共同步 " + total_count + " 条记录！");

				//$(".chart").draw;
				window.location.reload();
			});

			$(document).dequeue("ajaxRequests");
		});
	};
	
	setInterval(refreshDeviceStatus, 1 * 60 * 1000);
	setInterval(autoDownloadRecorders, 5 * 60 * 1000);
	
	$.plot(".chart", [], {
		grid: {
			borderWidth: 1,
			backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
		},
	});
	
	$.get(DATABASESERVICEURI + "getPlotData", {}, function (result) {
		//var data = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9], ["January1", 12], ["February1", 9], ["March1", 6], ["April1", 13], ["May1", 17], ["June1", 9]];
		$.plot(".chart",
			[{
				label: result.title, // "2012 Average Temperature",
				data: result.data,
				color: "#548000"
			}],
			{
				series: {
					bars: {
						show: true,
						barWidth: 0.6,
						lineWidth: 1,
						align: "center"
					}
				},
				xaxis: {
					mode: "categories",
					tickLength: 0,
				},
				yaxis:{
					tickDecimals: 0,
					tickSize: 5,
				},
				grid: {
					hoverable: true,
					borderWidth: 1,
					backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
				},
				legend: {
					labelBoxBorderColor: null, //"#000000",
					//position: "nw"
				}
			}
		);
		$(".chart").UseTooltip();
	});

	var previousPoint = null, previousLabel = null;

	$.fn.UseTooltip = function () {
		$(this).bind("plothover", function (event, pos, item) {
			if (item) {
				if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
					previousPoint = item.dataIndex;
					previousLabel = item.series.label;
					$("#tooltip").remove();
 
					var x = item.datapoint[0];
					var y = item.datapoint[1];
					var color = item.series.color;

					showTooltip(
								item.pageX,
								item.pageY,
								color,
								"<strong>" + "使用人次：" + item.series.data[item.dataIndex][1] + "</strong>");
				}
			} else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});
	};

	function showTooltip(x, y, color, contents) {
		$('<div id="tooltip">' + contents + '</div>').css({
			'position': 'absolute',
			'display': 'none',
			'top': y - 40,
			'left': x - 45,
			//'border': '1px solid ' + color,
			'padding': '8px',
			'font-size': '9px',
			'border-radius': '5px',
			'background-color': '#000',
			'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
			'opacity': 0.9
		}).appendTo("body").fadeIn(200);
	}
});