class Config:
	WIFI_SSID = ""
	WIFI_PASSWORD = ""

	UPDATE_TEMPERATURE_URI = "http://192.168.11.10/temperature/inc/api/v1/update"
	HEADERS = {
		"Content-Type": "application/x-www-form-urlencoded"
	}
	POST_DATA = "temp1=%s&temp2=%s&timestamp=%s"
	RESULT = "result"
	ERROR_CODE = "error_code"
	ERROR_MSG = "error_msg"
	NTP_HOST = "cn.ntp.org.cn"
