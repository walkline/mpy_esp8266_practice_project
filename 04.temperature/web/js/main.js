"use strict"

$(document).ready(function () {
	var echart_one = echarts.init(document.getElementById("echart_one"), "light");
	var echart_two = echarts.init(document.getElementById("echart_two"), "light");
	var echart_three = echarts.init(document.getElementById("echart_three"), "light");
	var data1 = [];
	var data2 = [];
	var data3 = [];
    var option = {
        title: {
			text: "1小时统计",
			left: "center"
        },
        tooltip: {
			trigger: "axis",
			axisPointer: {
				type: "line"
			}
		},
        legend: {
			data: ["温度"],
			right: 0,
			show: false
        },
        xAxis: {
			type: "time",
			//boundaryGap: false,
            data: ["10:00", "10:05", "10:10", "10:15", "10:20"]
        },
        yAxis: {
			type: "value",
			// interval: 4,
			axisLabel: {
				formatter: "{value} °C"
			}
        },
        series: [{
            name: "温度",
            type: "line",
			data: data1,
			animation: false,
			markPoint: {
                data: [
                    {type: 'max', name: '最大值'},
                    {type: 'min', name: '最小值'}
                ]
            },
			markLine: {
				data: [{
					type: "average"
				}]
			}
        }]
	};
	var option2 = {
		title: {
			text: "8小时统计"
		}
	};
	var option3 = {
		title: {
			text: "24小时统计"
		}
	}

    if (option && typeof(option) === "object") {
		echart_one.setOption(option, true);
		
		echart_two.setOption(option, true);
		echart_two.setOption(option2);

		echart_three.setOption(option, true);
		echart_three.setOption(option3);
    }

	$.get("http://walkline.wang/temperature/inc/api/v1/getPlotData", {}, function (response) {
		console.log(response);
		if (response.result === "success") {
			echart_one.setOption({
				series: [{
					data: response.data
				}, true]
			});
		}
	});

	$.get("http://walkline.wang/temperature/inc/api/v1/getPlotData", {range: 8}, function (response) {
		console.log(response);
		if (response.result === "success") {
			echart_two.setOption({
				series: [{
					data: response.data
				}, true]
			});
		}
	});
	
	$.get("http://walkline.wang/temperature/inc/api/v1/getPlotData", {range: 24}, function (response) {
		console.log(response);
		if (response.result === "success") {
			echart_three.setOption({
				series: [{
					data: response.data
				}, true]
			});
		}
	});

    window.onresize = function () {
		echart_one.resize();
		echart_two.resize();
		echart_three.resize();
	};
	
	setTimeout(function () {window.location.reload()}, 5 * 60 * 1000);
});