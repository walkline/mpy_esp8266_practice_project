/*
Navicat MySQL Data Transfer

Source Server         : remote
Source Server Version : 50148
Source Host           : qdm16522623.my3w.com:3306
Source Database       : qdm16522623_db

Target Server Type    : MYSQL
Target Server Version : 50148
File Encoding         : 65001

Date: 2018-12-30 22:45:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for temperature_recorders
-- ----------------------------
DROP TABLE IF EXISTS `temperature_recorders`;
CREATE TABLE `temperature_recorders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `temperature1` varchar(8) DEFAULT NULL,
  `temperature2` varchar(8) DEFAULT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
