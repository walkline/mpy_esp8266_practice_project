import timercontroller
from machine import WDT
import utils
import network


watch_dog = WDT()
timer_controller = timercontroller.TimerController()


if __name__ == "__main__":
	utils.read_log()
	watch_dog.feed()

	try:
		timer_controller.start_wifi_status_timer()

		result_code = utils.connect_to_internet()

		if result_code == network.STAT_GOT_IP:
			# for count in range(3):
			#	watch_dog.feed()

			#	if utils.sync_time():
			#		break
			# else:
			#	utils.reset()

			timer_controller.start_get_temperature_timer()  # period=5000)
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET DEVICE")
	except Exception as e:
		utils.log(e)
