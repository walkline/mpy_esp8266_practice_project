from machine import Pin
from onewire import OneWire
from ds18x20 import DS18X20
from utime import sleep_ms


class DS18B20Exception(BaseException):
	pass


class DS18B20:
	def __init__(self, dataline: int):
		self._oneware = OneWire(Pin(dataline))
		self._ds18b20 = DS18X20(self._oneware)

	def deinit(self):
		self._ds18b20 = None
		self._oneware = None

	def get_temperature(self):
		roms = self._ds18b20.scan()
		self._ds18b20.convert_temp()

		sleep_ms(750)

		if len(roms) > 0:
			temp = round(self._ds18b20.read_temp(roms[0]), 1)

			return temp
		else:
			return None
