from machine import Pin
import dht


class DHT11:
	def __init__(self, dataline: int):
		self._dht11 = dht.DHT11(Pin(dataline))

	def deinit(self):
		self._dht11 = None

	def get_temperature(self):
		self._dht11.measure()

		return self._dht11.temperature()
